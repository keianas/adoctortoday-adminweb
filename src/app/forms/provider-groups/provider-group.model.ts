﻿export class ProviderGroup
{
    constructor(public name: string,
        public address: string,
        public primaryContactName: string,
        public primaryContactEmail: string,
        public primaryContactPhone: string,
        public signUpDate: string,
        public Insurance: string,
        public paymentFreq: string,
        public paymentAmount: string,
        public perApptAmount: string,
        public paymentMethod: string,
        public lastPaymentdate: string,
        public ratingEnabled: boolean)
    {
       
    }


}