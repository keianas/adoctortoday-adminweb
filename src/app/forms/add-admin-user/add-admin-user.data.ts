﻿import { Select2OptionData } from 'ng2-select2/ng2-select2';

export const select2DefaultData: Select2OptionData[] = [
    {
        "text": "Alabama",
        "id": "AL"
    },
    {
        "text": "Alaska",
        "id": "AK"
    },
    {
        "text": "Arizona",
        "id": "AZ"
    },
    {
        "text": "Arkansas",
        "id": "AR"
    },
    {
        "text": "California",
        "id": "CA"
    },
    {
        "text": "Colorado",
        "id": "CO"
    },
    {
        "text": "Connecticut",
        "id": "CT"
    },
    {
        "text": "Delaware",
        "id": "DE"
    },
    {
        "text": "District Of Columbia",
        "id": "DC"
    },
    {
        "text": "Florida",
        "id": "FL"
    },
    {
        "text": "Georgia",
        "id": "GA"
    },
    {
        "text": "Hawaii",
        "id": "HI"
    },
    {
        "text": "Idaho",
        "id": "ID"
    },
    {
        "text": "Illinois",
        "id": "IL"
    },
    {
        "text": "Indiana",
        "id": "IN"
    },
    {
        "text": "Iowa",
        "id": "IA"
    },
    {
        "text": "Kansas",
        "id": "KS"
    },
    {
        "text": "Kentucky",
        "id": "KY"
    },
    {
        "text": "Louisiana",
        "id": "LA"
    },
    {
        "text": "Maine",
        "id": "ME"
    },
    {
        "text": "Maryland",
        "id": "MD"
    },
    {
        "text": "Massachusetts",
        "id": "MA"
    },
    {
        "text": "Michigan",
        "id": "MI"
    },
    {
        "text": "Minnesota",
        "id": "MN"
    },
    {
        "text": "Mississippi",
        "id": "MS"
    },
    {
        "text": "Missouri",
        "id": "MO"
    },
    {
        "text": "Montana",
        "id": "MT"
    },
    {
        "text": "Nebraska",
        "id": "NE"
    },
    {
        "text": "Nevada",
        "id": "NV"
    },
    {
        "text": "New Hampshire",
        "id": "NH"
    },
    {
        "text": "New Jersey",
        "id": "NJ"
    },
    {
        "text": "New Mexico",
        "id": "NM"
    },
    {
        "text": "New York",
        "id": "NY"
    },
    {
        "text": "North Carolina",
        "id": "NC"
    },
    {
        "text": "North Dakota",
        "id": "ND"
    },
    {
        "text": "Ohio",
        "id": "OH"
    },
    {
        "text": "Oklahoma",
        "id": "OK"
    },
    {
        "text": "Oregon",
        "id": "OR"
    },
    {
        "text": "Pennsylvania",
        "id": "PA"
    },
    {
        "text": "Puerto Rico",
        "id": "PR"
    },
    {
        "text": "Rhode Island",
        "id": "RI"
    },
    {
        "text": "South Carolina",
        "id": "SC"
    },
    {
        "text": "South Dakota",
        "id": "SD"
    },
    {
        "text": "Tennessee",
        "id": "TN"
    },
    {
        "text": "Texas",
        "id": "TX"
    },
    {
        "text": "Utah",
        "id": "UT"
    },
    {
        "text": "Vermont",
        "id": "VT"
    },
    {
        "text": "Virginia",
        "id": "VA"
    },
    {
        "text": "Washington",
        "id": "WA"
    },
    {
        "text": "West Virginia",
        "id": "WV"
    },
    {
        "text": "Wisconsin",
        "id": "WI"
    },
    {
        "text": "Wyoming",
        "id": "WY"
    }];


export const select2GroupedData: Select2OptionData[] = [{
  text: '1',
  id: 'NFC EAST',
  children: [
    {
      text: '11',
      id: 'Dallas Cowboys'
    },
    {
      text: '12',
      id: 'New York Giants'
    },
    {
      text: '13',
      id: 'Philadelphia Eagles'
    },
    {
      text: '14',
      id: 'Washington Redskins'
    }
  ]
}, {
  text: '2',
  id: 'NFC NORTH',
  children: [
    {
      text: '21',
      id: 'Chicago Bears'
    },
    {
      text: '22',
      id: 'Detroit Lions'
    },
    {
      text: '23',
      id: 'Green Bay Packers'
    },
    {
      text: '24',
      id: 'Minnesota Vikings'
    }
  ]
}, {
  text: '3',
  id: 'NFC SOUTH',
  children: [
    {
      text: '31',
      id: 'Atlanta Falcons'
    },
    {
      text: '32',
      id: 'Carolina Panthers'
    },
    {
      text: '33',
      id: 'New Orleans Saints'
    },
    {
      text: '34',
      id: 'Tampa Bay Buccaneers'
    }
  ]
}];
