﻿export class AdminUser
{
    public firstName: string;
    public lastName: string;
    public address: string;
    public address2: string;
    public city: string;
    public state: string;
    public zipcode: string;
    public email: string;
    public officePhone: string;
    public cellPhone: string;

    constructor(firstName: string, lastName: string, address: string, address2: string, city: string, state: string, zipcode: string, email: string, officePhone: string, cellPhone: string)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.email = email;
        this.officePhone = officePhone;
        this.cellPhone = cellPhone;
    }


}